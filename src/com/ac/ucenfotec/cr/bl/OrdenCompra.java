package com.ac.ucenfotec.cr.bl;

import java.time.LocalDate;

public class OrdenCompra {
    private Persona nombreGanador;
    private LocalDate fecha;
    private Item detalleObjeto;
    private Oferta precio;

    OrdenCompra(){

    }

    public OrdenCompra(LocalDate fecha) {
        this.fecha = fecha;
    }

    public OrdenCompra(Persona nombreGanador, LocalDate fecha, Item detalleObjeto, Oferta precio) {
        this.nombreGanador = nombreGanador;
        this.fecha = fecha;
        this.detalleObjeto = detalleObjeto;
        this.precio = precio;
    }

    public Persona getNombreGanador() {
        return nombreGanador;
    }

    public void setNombreGanador(Persona nombreGanador) {
        this.nombreGanador = nombreGanador;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Item getDetalleObjeto() {
        return detalleObjeto;
    }

    public void setDetalleObjeto(Item detalleObjeto) {
        this.detalleObjeto = detalleObjeto;
    }

    public Oferta getPrecio() {
        return precio;
    }

    public void setPrecio(Oferta precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "OrdenCompra{" +
                "nombreGanador=" + nombreGanador.toString() +
                ", fecha=" + fecha +
                ", detalleObjeto=" + detalleObjeto.toString() +
                ", precio=" + precio.toString() +
                '}';
    }
}
