package com.ac.ucenfotec.cr.bl;

import java.time.LocalDate;

public class Vendedor extends Persona{
    double puntuacion;
    String provincia;
    String canton;
    String distrito;
    String direccionExacta;

    public Vendedor() {
        super();
    }

    public Vendedor(String nombre, String nombre2, String apellido, String apellido2, String identificacion, LocalDate fechaNacimiento, int edad, String contrasenna, String correo, double puntuacion, String provincia, String canton, String distrito, String direccionExacta) {
        super(nombre, nombre2, apellido, apellido2, identificacion, fechaNacimiento, edad, contrasenna, correo);
        this.puntuacion = puntuacion;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.direccionExacta = direccionExacta;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "puntuacion=" + puntuacion +
                ", provincia='" + provincia + '\'' +
                ", canton='" + canton + '\'' +
                ", distrito='" + distrito + '\'' +
                ", direccionExacta='" + direccionExacta + '\'' +
                ", nombre='" + nombre + '\'' +
                ", nombre2='" + nombre2 + '\'' +
                ", apellido='" + apellido + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", contrasenna='" + contrasenna + '\'' +
                ", correo='" + correo + '\'' +
                '}';
    }
}
