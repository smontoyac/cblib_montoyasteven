package com.ac.ucenfotec.cr.bl;

import java.time.LocalDate;

public class Subasta {
    private LocalDate fecha;
    private Item objeto;
    private Persona nombreVendedor;
    private double puntuacion;
    private double precio;

    public Subasta(){

    }

    public Subasta(LocalDate fecha, Persona nombreVendedor, double puntuacion, double precio) {
        this.fecha = fecha;
        this.objeto = objeto;
        this.nombreVendedor = nombreVendedor;
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public Subasta(LocalDate fecha, Item objeto, Persona nombreVendedor, double puntuacion, double precio) {
        this.fecha = fecha;
        this.objeto = objeto;
        this.nombreVendedor = nombreVendedor;
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Item getObjeto() {
        return objeto;
    }

    public void setObjeto(Item objeto) {
        this.objeto = objeto;
    }

    public Persona getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(Persona nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Subasta{" +
                "fecha=" + fecha +
                ", objeto=" + objeto.toString() +
                ", nombreVendedor=" + nombreVendedor.toString() +
                ", puntuacion=" + puntuacion +
                ", precio=" + precio +
                '}';
    }
}
