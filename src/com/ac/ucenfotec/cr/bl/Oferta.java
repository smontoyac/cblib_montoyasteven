package com.ac.ucenfotec.cr.bl;

public class Oferta {
    private Persona nombreOferente;
    private double puntuacion;
    private double precio;

    Oferta(){

    }

    public Oferta(double puntuacion, double precio) {
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public Oferta(Persona nombreOferente, double puntuacion, double precio) {
        this.nombreOferente = nombreOferente;
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public Persona getNombreOferente() {
        return nombreOferente;
    }

    public void setNombreOferente(Persona nombreOferente) {
        this.nombreOferente = nombreOferente;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Oferta{" +
                "nombreOferente=" + nombreOferente.toString() +
                ", puntuacion=" + puntuacion +
                ", precio=" + precio +
                '}';
    }
}
